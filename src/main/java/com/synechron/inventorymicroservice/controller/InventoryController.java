package com.synechron.inventorymicroservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
@RequiredArgsConstructor
@Slf4j
public class InventoryController {

    private static long counter = 1000;

    private final Environment environment;

    @PostMapping
    public Long updateQuantity(){
        log.info("Came inside the update method :: {}", this.environment.getProperty("server.port"));
        return --counter;
    }
}