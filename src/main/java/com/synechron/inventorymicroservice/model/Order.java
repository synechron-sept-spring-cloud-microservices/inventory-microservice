package com.synechron.inventorymicroservice.model;

import lombok.*;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = {"orderId", "price"})
public class Order {
    private long orderId;

    private double price;

    private LocalDate orderDate;
}